class MarsExploration {

  rovers = [];
  results = [];

  definePlateauSize(name, upperRightCoordinates) {

    const [ upperX, upperY ] = upperRightCoordinates.split(',');

    return {
      name: name,
      upperX: +upperX,
      upperY: +upperY
    }
  }

  landRover(roverId, name, landingPosition) {

    let [ x, y, direction ] = landingPosition.split(' ');
    let xAxis = +x;
    let yAxis = +y;

    const rover = {
      id: roverId,
      name,
      positionX: xAxis,
      posiitionY: yAxis,
      orientation: direction
    }

    this.rovers.push(rover);

    return rover;
  }

  moveRover(roverId, commands) {

    const directions = ['N', 'E', 'S', 'W'];

    const { id, positionX, posiitionY, orientation } = this.rovers.find(el => el.id === roverId);

    let xAxis = positionX;
    let yAxis = posiitionY;

    let directionIndex = directions.indexOf(orientation);

    const handleDirection = (el) => {
      if(el === 'R'){

        if(directionIndex === 3){
          directionIndex = 0;  
        } else {
          directionIndex ++;
        }

      }

      if(el === 'L'){

        if( directionIndex === 0){
          directionIndex = 3;  
        } else {
          directionIndex --;
        }
      }
    }

    const handlePosition = (el, direction) => {

      if(el === 'M'){

        if(direction === 'N'){
          yAxis++;
        }
  
        if(direction === 'E'){
          xAxis++;
        }
  
        if(direction === 'S'){
          yAxis--;
        }
  
        if(direction === 'W'){
          xAxis--;
        }
      }

    }

    commands.split('').forEach((el) => {
      handleDirection(el);
      handlePosition(el, directions[directionIndex]);
    });

    this.results.push({
      id,
      newPositionX: xAxis,
      newPositionY: yAxis,
      newOrientation: directions[directionIndex]
    })

    return {
      newPositionX: xAxis,
      newPositionY: yAxis,
      newOrientation: directions[directionIndex]
    }
  }

  getRoverPositionMessage(roverId) {
    const { name } = this.rovers.find(el => el.id === roverId);
    const { newPositionX, newPositionY, newOrientation  } = this.results.find(el => el.id === roverId);

    return `Rover "${name}" is now at position ${newPositionX} ${newPositionY} ${newOrientation}`;
  }

  //initial logic, disregard function below
  calculate(initial, moviments){

    const directions = ['N', 'E', 'S', 'W'];

    let [ x, y, direction ] = initial.split(' ');
    let xAxis = +x;
    let yAxis = +y;

    let directionIndex = directions.indexOf(direction);

    const handleDirection = (el) => {
      if(el === 'R'){

        if(directionIndex === 3){
          directionIndex = 0;  
        } else {
          directionIndex ++;
        }

      }

      if(el === 'L'){

        if( directionIndex === 0){
          directionIndex = 3;  
        } else {
          directionIndex --;
        }
      }
    }

    const handlePosition = (el, direction) => {

      if(el === 'M'){

        if(direction === 'N'){
          yAxis++;
        }
  
        if(direction === 'E'){
          xAxis++;
        }
  
        if(direction === 'S'){
          yAxis--;
        }
  
        if(direction === 'W'){
          xAxis--;
        }
      }

    }

    moviments.split('').forEach((el) => {
      handleDirection(el);
      handlePosition(el, directions[directionIndex]);
    });

    console.log(`Rover "Eagle" is now at position ${xAxis}, ${yAxis}, ${directions[directionIndex]}`);
  }
  
}

module.exports = MarsExploration;